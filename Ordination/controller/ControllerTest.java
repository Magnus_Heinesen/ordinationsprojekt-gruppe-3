package controller;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	private Controller c1;
	private Patient p1;
	private Patient p2;
	private Patient p3;
	private Laegemiddel l1;
	private PN PNordi;
	private DagligFast df1;
	private DagligSkaev ds1;

	@Before
	public void setUp() throws Exception {
		c1 = Controller.getController();
	}

	@Test
	public void testOpretPNOrdination() {
		p1 = new Patient("123456-7890", "Hans Hansen", 80.0);
		l1 = new Laegemiddel("Acetylsalicylsyre", 2, 4, 8, "Styk");

		// tc1
		PNordi = c1.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 7), p1, l1, 5);
		assertNotNull(PNordi);

		// tc2
		try {
			PNordi = c1.opretPNOrdination(LocalDate.of(2020, 3, 7), LocalDate.of(2020, 3, 2), p1, l1, 5);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "StartDato kan ikke være efter slutDato");

		}

	}

	@Test
	public void testOpretDagligFastOrdination() {
		p1 = new Patient("123456-7890", "Hans Hansen", 80.0);
		l1 = new Laegemiddel("Acetylsalicylsyre", 2, 4, 8, "Styk");

		// tc1
		df1 = c1.opretDagligFastOrdination(LocalDate.of(2020, 2, 17), LocalDate.of(2020, 2, 19), p1, l1, 2, 0, 3, 1);
		assertNotNull(df1);

		// tc2
		df1 = c1.opretDagligFastOrdination(LocalDate.of(2019, 12, 27), LocalDate.of(2020, 1, 2), p1, l1, 2, 0, 0, 1);
		assertNotNull(df1);

		// tc3
		try {
			df1 = c1.opretDagligFastOrdination(LocalDate.of(2020, 5, 17), LocalDate.of(2020, 2, 3), p1, l1, 2, 0, 3, 1);

		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "StartDato kan ikke være efter slutDato");
		}

	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		p1 = c1.opretPatient("123456-7890", "Hans Hansen", 80.0);
		l1 = c1.opretLaegemiddel("Acetylsalicylsyre", 2, 4, 8, "Styk");
		LocalTime[] k1 = { LocalTime.of(12, 00), LocalTime.of(13, 00) };
		double[] an = { 2, 1 };

		// tc1
		ds1 = c1.opretDagligSkaevOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 7), p1, l1, k1, an);
		assertNotNull(ds1);

		// tc2
		try {
			ds1 = c1.opretDagligSkaevOrdination(LocalDate.of(2020, 3, 7), LocalDate.of(2020, 3, 2), p1, l1, k1, an);

		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Start dato skal være før slut dato");
		}

	}

	@Test
	public void testOrdinationPNAnvendt() {
		p1 = c1.opretPatient("123456-7890", "Hans Hansen", 80.0);
		l1 = c1.opretLaegemiddel("Acetylsalicylsyre", 2, 4, 8, "Styk");
		PNordi = c1.opretPNOrdination(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 7), p1, l1, 5);

		// tc1
		assertTrue(PNordi.getAntalGangeGivet() == 0);
		c1.ordinationPNAnvendt(PNordi, LocalDate.of(2020, 3, 4));
		assertTrue(PNordi.getAntalGangeGivet() == 1);

		// tc2
		assertTrue(PNordi.getAntalGangeGivet() == 1);
		c1.ordinationPNAnvendt(PNordi, LocalDate.of(2020, 2, 2));
		assertTrue(PNordi.getAntalGangeGivet() == 1);

		try {
			c1.ordinationPNAnvendt(PNordi, LocalDate.of(2020, 2, 2));

		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "StartDato kan ikke være efter slutDato");
		}

	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		l1 = c1.opretLaegemiddel("Acetylsalicylsyre", 2, 4, 8, "Styk");
		p1 = c1.opretPatient("123456-7890", "Hans Hansen", 80.0);
		p2 = c1.opretPatient("09876-4321", "Jens Jensen", 20.0);
		p3 = c1.opretPatient("123412-1234", "Lars Larsen", 122.0);

		// tc1
		assertEquals(320, c1.anbefaletDosisPrDoegn(p1, l1), 0.01);
		// tc2
		assertEquals(40, c1.anbefaletDosisPrDoegn(p2, l1), 0.01);
		// tc3
		assertEquals(976, c1.anbefaletDosisPrDoegn(p3, l1), 0.01);

	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		p1 = c1.opretPatient("123456-7890", "Hans Hansen", 24.0);
		p2 = c1.opretPatient("123456-4865", "Jens Jensen", 80.0);
		p3 = c1.opretPatient("123456-7246", "Lars Larsen", 121.0);
		l1 = c1.opretLaegemiddel("Acetylsalicylsyre", 2, 4, 8, "Styk");

		// tc1
		c1.opretPNOrdination(LocalDate.of(2020, 3, 3), LocalDate.of(2020, 3, 7), p1, l1, 2);
		assertEquals(1, c1.antalOrdinationerPrVægtPrLægemiddel(24.0, 70, l1));
		// tc2
		c1.opretPNOrdination(LocalDate.of(2020, 3, 3), LocalDate.of(2020, 3, 7), p2, l1, 2);
		assertEquals(2, c1.antalOrdinationerPrVægtPrLægemiddel(24.0, 80, l1));
		// tc3
		c1.opretPNOrdination(LocalDate.of(2020, 3, 3), LocalDate.of(2020, 3, 7), p3, l1, 2);
		assertEquals(2, c1.antalOrdinationerPrVægtPrLægemiddel(24.0, 80, l1));

	}

}
