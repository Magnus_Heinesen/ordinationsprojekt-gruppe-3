package ordination;

import java.time.*;
import java.util.Arrays;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];
	private LocalTime morgen = LocalTime.of(8, 0);
	private LocalTime middag = LocalTime.of(14, 0);
	private LocalTime aften = LocalTime.of(20, 0);
	private LocalTime nat = LocalTime.of(2, 0);

	public DagligFast(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, patient);
		createDosis(morgen, morgenAntal);
		createDosis(middag, middagAntal);
		createDosis(aften, aftenAntal);
		createDosis(nat, natAntal);
	}

	public Dosis[] getDoser() {
		return Arrays.copyOf(doser, doser.length);
	}

	@Override
	public double doegnDosis() {
		double sum = 0;

		for (Dosis d : doser) {
			sum += d.getAntal();
		}

		return sum;
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * super.antalDage();
	}

	@Override
	public String getType() {
		return "DagligFast";
	}

	public Dosis[] createDosis(LocalTime tid, double antal) {
		if (tid.isBefore(LocalTime.of(6, 00))) {
			doser[3] = new Dosis(tid, antal);
		} else if (tid.isBefore(LocalTime.of(12, 00))) {
			doser[0] = new Dosis(tid, antal);
		} else if (tid.isBefore(LocalTime.of(18, 00))) {
			doser[1] = new Dosis(tid, antal);
		} else {
			doser[2] = new Dosis(tid, antal);
		}
		return doser;
	}
}