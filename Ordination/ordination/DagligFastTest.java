package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DagligFastTest {

	private Patient p1;
	private Laegemiddel l1;
	private DagligFast df1;


	@Before
	public void setUp() throws Exception {
		p1 = new Patient("123456-7890", "Hans Hansen", 80.0);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		df1 = new DagligFast(LocalDate.of(2020, 03, 02), LocalDate.of(2020, 03, 9), p1, l1, 2, 3, 1, 0);
	}

	@Test
	public void testDoegnDosis() {
		DagligFast df2 = new DagligFast(LocalDate.of(2020, 03, 04), LocalDate.of(2020, 03, 07), p1, l1, 2, 3, 1, 0);
		// TC1
		df2.createDosis(LocalTime.of(12, 00), 2);
		assertEquals(5, df2.doegnDosis(), 0.00001);	
		// TC2
		df2.createDosis(LocalTime.of(15, 00), 0);
		assertEquals(3, df2.doegnDosis(), 0.00001);
		// TC3 
		df2.createDosis(LocalTime.of(14, 00), 2);
		assertEquals(5, df2.doegnDosis(), 0.00001);
		
	}

	@Test
	public void testCreateDosis() {
		// TC1
		df1.createDosis(LocalTime.of(23, 59), 2);
		assertEquals(2, df1.getDoser()[2].getAntal(), 0.0001);
		
		assertEquals(2, df1.getDoser()[0].getAntal(), 0.0001);
		assertEquals(3, df1.getDoser()[1].getAntal(), 0.0001);
		assertEquals(0, df1.getDoser()[3].getAntal(), 0.0001);
		
		// TC2
		df1.createDosis(LocalTime.of(00, 00), 2);
		assertEquals(2, df1.getDoser()[3].getAntal(), 0.0001);

		// TC3
		df1.createDosis(LocalTime.of(05, 59), 2);
		assertEquals(2, df1.getDoser()[3].getAntal(), 0.0001);
		
		// TC4
		df1.createDosis(LocalTime.of(06, 00), 2);
		assertEquals(2, df1.getDoser()[0].getAntal(), 0.0001);
		
		// TC5
		df1.createDosis(LocalTime.of(11, 59), 2);
		assertEquals(2, df1.getDoser()[0].getAntal(), 0.0001);
		
		// TC6
		df1.createDosis(LocalTime.of(12, 00), 2);
		assertEquals(2, df1.getDoser()[1].getAntal(), 0.0001);
		
		// TC7
		df1.createDosis(LocalTime.of(17, 59), 2);
		assertEquals(2, df1.getDoser()[1].getAntal(), 0.0001);
		
		// TC8
		df1.createDosis(LocalTime.of(18, 00), 2);
		assertEquals(2, df1.getDoser()[2].getAntal(), 0.0001);
		
		// TC9
		df1.createDosis(LocalTime.of(07, 00), 8);
		assertEquals(8, df1.getDoser()[0].getAntal(), 0.0001);
	}
}