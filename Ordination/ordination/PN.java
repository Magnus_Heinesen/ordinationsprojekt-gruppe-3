package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	public PN(LocalDate startDen, LocalDate slutDen, Patient patient, double antalEnheder) {
		super(startDen, slutDen, patient);
		this.antalEnheder = antalEnheder;

	}

	private double antalEnheder;

	private ArrayList<LocalDate> givetDosis = new ArrayList<>();

	public ArrayList<LocalDate> getGivetDosis() {
		return new ArrayList <>(givetDosis);
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean var = false;
		if (givesDen.compareTo(getStartDen()) >= 0 && givesDen.compareTo(getSlutDen()) <= 0) {

			givetDosis.add(givesDen);
			var = true;
		}
		return var;
	}

	public double doegnDosis() {
		int doegnTæller = 0;
		if (givetDosis.size() == 0) {
			doegnTæller = 0;
		} else

			doegnTæller = (int) ChronoUnit.DAYS.between(givetDosis.get(0), sidsteDato());

		if (doegnTæller < 1) {
			doegnTæller = 1;
		}

		return samletDosis() / doegnTæller;
	}

	public LocalDate sidsteDato() {
		LocalDate temp = null;
		for (LocalDate d : givetDosis) {
			temp = d;
		}
		return temp;
	}

	public double samletDosis() {
		return givetDosis.size() * antalEnheder;

	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		int sum = givetDosis.size();
		return sum;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {

		return "PN";
	}

}
