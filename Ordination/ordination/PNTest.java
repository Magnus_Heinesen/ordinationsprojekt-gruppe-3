package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PNTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSamletDosis() {
		Patient p1 = new Patient("123456-7890", "Hans Hansen", 80.0);
		PN pn1 = new PN(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 7), p1, 5);
		PN pn2 = new PN(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 7), p1, 5);
		PN pn3 = new PN(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 7), p1, 5);
		//TC1
		pn1.givDosis(LocalDate.of(2020, 3, 5));
		assertEquals(5, pn1.samletDosis(), 0);
		assertEquals(1, pn1.getGivetDosis().size());
		assertTrue(pn1.getGivetDosis().contains(LocalDate.of(2020, 3, 5)));
		//TC2
		pn2.givDosis(LocalDate.of(2020, 3, 5));
		pn2.givDosis(LocalDate.of(2020, 3, 6));
		pn2.givDosis(LocalDate.of(2020, 3, 7));
		assertEquals(15, pn2.samletDosis(), 0);
		assertTrue(pn2.getGivetDosis().contains(LocalDate.of(2020, 3, 5)));
		assertTrue(pn2.getGivetDosis().contains(LocalDate.of(2020, 3, 6)));
		assertTrue(pn2.getGivetDosis().contains(LocalDate.of(2020, 3, 7)));
		
		//TC3
		pn3.givDosis(LocalDate.of(2020, 2, 28));
		assertEquals(0, pn3.samletDosis(), 0);
		assertFalse(pn3.getGivetDosis().contains(LocalDate.of(2020, 2, 28)));
	}

	@Test
	public void testDoegnDosis() {
		Patient p1 = new Patient("123456-7890", "Hans Hansen", 80.0);
		PN pn1 = new PN(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 7), p1, 5);
		PN pn2 = new PN(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 7), p1, 5);
		PN pn3 = new PN(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 7), p1, 5);
		// TC1
		pn1.givDosis(LocalDate.of(2020, 3, 7));
		assertEquals(5, pn1.doegnDosis(), 0);
		assertTrue(pn1.getGivetDosis().contains(LocalDate.of(2020, 3, 7)));
		// TC2
		pn2.givDosis(LocalDate.of(2020, 3, 6));
		pn2.givDosis(LocalDate.of(2020, 3, 6));
		pn2.givDosis(LocalDate.of(2020, 3, 6));
		assertEquals(15, pn2.doegnDosis(), 0);
		assertTrue(pn2.getGivetDosis().contains(LocalDate.of(2020, 3, 6)));
		// TC3
		pn3.givDosis(LocalDate.of(2020, 3, 6));
		assertFalse(pn3.givDosis(LocalDate.of(2020, 3, 8)));
		assertEquals(5, pn3.doegnDosis(), 0);
		assertTrue(pn3.getGivetDosis().contains(LocalDate.of(2020, 3, 6)));
		

	}

	@Test
	public void testGivDosis() {
		Patient p1 = new Patient("123456-7890", "Hans Hansen", 80.0);
		PN pn1 = new PN(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 9), p1, 2);
		// TC1
		assertTrue(pn1.givDosis(LocalDate.of(2020, 3, 4)));
		assertEquals(1, pn1.getAntalGangeGivet());
		assertTrue(pn1.getGivetDosis().contains(LocalDate.of(2020, 3, 4)));
		// TC2
		assertFalse(pn1.givDosis(LocalDate.of(2020, 2, 25)));
		assertEquals(1, pn1.getAntalGangeGivet());
		assertFalse(pn1.getGivetDosis().contains(LocalDate.of(2020, 2, 25)));

	}

}
