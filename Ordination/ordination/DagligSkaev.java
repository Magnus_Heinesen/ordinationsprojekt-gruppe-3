package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Patient patient) {
		super(startDen, slutDen, patient);
		// TODO Auto-generated constructor stub
	}

	public void opretDosis(LocalTime tid, double antal) {
		// TODO
		Dosis d = new Dosis(tid, antal);
		doser.add(d);

	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}

	@Override
	public double samletDosis() {
		// TODO Auto-generated method stub
		int sum = 0;

		for (Dosis d : doser) {
			sum += d.getAntal();
		}
		return sum;
	}

	@Override
	public double doegnDosis() {
		// TODO Auto-generated method stub
		int sum = 0;

		for (Dosis d : doser) {
			sum += d.getAntal();
		}
		sum /= ChronoUnit.DAYS.between(this.getStartDen(), this.getSlutDen());

		return sum;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Daglig skæv dosis";
	}

}
