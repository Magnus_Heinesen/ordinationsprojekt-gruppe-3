package ordination;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {

	private Patient p1;
	private Laegemiddel l1;
	private DagligSkaev ds1;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("123456-7890", "Hans Hansen", 80.0);
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

	}

	@Test
	public void testDoegnDosis() {
		l1 = new Laegemiddel("Acetylsalicylsyre", 2, 4, 8, "Styk");
		ds1 = new DagligSkaev(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 3, 7), p1);

		//tc1
		ds1.opretDosis(LocalTime.of(10, 00), 4);
		ds1.opretDosis(LocalTime.of(11, 30), 2);
		assertEquals(2, ds1.doegnDosis(), 0.01);
		
		//tc2
		ds1.opretDosis(LocalTime.of(16, 30), 3);
		assertEquals(3, ds1.doegnDosis(), 0.01);

	}

	@Test
	public void testOpretDosis() {
		ds1 = new DagligSkaev(LocalDate.of(2020, 3, 3), LocalDate.of(2020, 3, 9), p1);

		// tc1
		assertTrue(ds1.getDoser().size() == 0);
		ds1.opretDosis(LocalTime.of(12, 50), 5.0);
		assertTrue(ds1.getDoser().size() == 1);
		System.out.println(ds1.getDoser());

		// tc2
		assertTrue(ds1.getDoser().size() == 1);
		ds1.opretDosis(LocalTime.of(13, 00), 5.0);
		assertTrue(ds1.getDoser().size() == 2);
		System.out.println(ds1.getDoser());

		// tc3
//		assertTrue(ds1.getDoser().size() == 2);
//		ds1.opretDosis(6.5, LocalTime.of(11, 30));
//		
	}

}
