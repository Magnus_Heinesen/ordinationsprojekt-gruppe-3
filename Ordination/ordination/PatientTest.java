package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PatientTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAddOrdination() {
		Patient p1 = new Patient("123456-7890", "Hans Hansen", 80.0);
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 9), p1);
		PN pn1 = new PN(LocalDate.of(2020,3,2), LocalDate.of(2020, 03, 9), p1, 2);
		//TC1
		p1.addOrdination(ds1);
		assertTrue(p1.getOrdinationer().contains(ds1));
		//TC2
		p1.addOrdination(pn1);
		assertTrue(p1.getOrdinationer().contains(pn1));
		//TC3
		//p1.addOrdination(pn);
		
	}

}
