package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class OrdinationTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSetLaegemiddel() {
		Patient p1 = new Patient("123456-7890", "Hans Hansen", 80.0);
		Laegemiddel l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		Laegemiddel l2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 9), p1);
		//TC1
		ds1.setLaegemiddel(l2);
		assertTrue(ds1.getLaegemiddel().equals(l2));
		//TC2
		ds1.setLaegemiddel(l1);
		assertTrue(ds1.getLaegemiddel().equals(l1));
		//TC3
		//ds1.setLaegemiddel(p1);
	}

}
